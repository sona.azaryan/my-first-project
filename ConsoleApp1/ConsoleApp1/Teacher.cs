﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Teacher
    {
        public Teacher(string name, string surname, string email, string qualification, int age)

        {
            Name = name;
            Surname = surname;
            Email = email;
            Age = age;
            Qualification = qualification;
        }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Qualification { get; set; }
        public int Age { get; set; }
    }
}
