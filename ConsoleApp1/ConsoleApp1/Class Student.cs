﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Student
    {
        public Student(string name, string surname, string email, int age)

        {
            Name = name;
            Surname = surname;
            Email = email;
            Age = age;
        }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }



    }
}

