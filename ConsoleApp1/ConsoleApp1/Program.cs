﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = CreateStudents(55);

            Print(students);

            Console.WriteLine();

            List<Teacher> teachers = CreateTeachers(7);
           // Print(teachers);


            List<Group> groups = CreateGroups("C#", teachers, students);

            //Print(groups);

            Console.ReadKey();
        }
        static List<Student> CreateStudents(int count)
        {
            List<Student> list = new List<Student>(count);
            for (int i = 0; i < list.Count; i++)
            {
                list.Add(new Student(

                    $"N{i + 1}",

                    $"S{i + 1}yan",

                    $"M{i + 1}@gmail.com",


                    i + 1));

            }

            return list;
        }
        static List<Teacher> CreateTeachers(int count)

        {
            List<Teacher> teachers = new List<Teacher>(count);
            for (int i = 0; i < count; i++)

            {
                teachers.Add(new Teacher(
                    $"TN{i + 1}",
                    $"TS{i + 1}yan",
                    $"TM{i + 1}@gmail.com",
                    $"Q{i + 1}",
                    i+1
                    ));
            }
            return teachers;
        }
        static void Print(List<Student> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine($"{list[i].Name}, {list[i].Surname}, {list[i].Email}, {list[i].Age}");
            }
        }
        static List<Group> CreateGroups(string groupName, List<Teacher> teachers, List<Student> students)
        {
            int groupCount = teachers.Count;

            List<Group> groups = new List<Group>(groupCount);

            foreach (Teacher teacher in teachers)

            {

                Group group = new Group();

                group.Name = groupName;

                group.Teacher = teacher;

                groups.Add(group);

            }

            return groups;

        }
    }
}
